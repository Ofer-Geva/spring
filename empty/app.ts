require('dotenv').config();
const PORT = process.env.PORT;
import { Application, json, Request, Response } from 'express';
import cors = require('cors');
const app: Application = require('express')();
import {Logger} from 'shared_spring';

app.use(json());
app.use(cors());

app.get('/', (req: Request, res: Response) => { res.send({ healthCheck: true }); });

app.listen(PORT,() => { Logger.info(`listening on port listening on port ${PORT}`); });