import CommonController from "../../controllers/common";

test("When runnin commonFunction should return an exponent of the given parameters", () =>{
    expect(CommonController.commonFunction(3,2)).toBe(9);
    expect(CommonController.commonFunction(3,0)).toBe(1);
});