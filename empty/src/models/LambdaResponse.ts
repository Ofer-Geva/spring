export default class LambdaResponse {
    statusCode: number;
    body: object;
    headers: object;

    constructor(statusCode: number, body: object, headers: object) {
        if(statusCode) this.statusCode = statusCode;
        if(body) this.body = body;
        if(headers) this.headers = headers;
    }

    parseToDictionary = () => {
        return {
            statusCode: this.statusCode,
            body: this.body,
            headers: this.headers
        }
    }
}