import asyncHooks = require('async_hooks');
import { v4 } from 'uuid';
const store = new Map();

const asyncHook = asyncHooks.createHook({
    init: (asyncId: number, _: string, triggerAsyncId: number): void => {
        if (store.has(triggerAsyncId)) {
            store.set(asyncId, store.get(triggerAsyncId))
        }
    },
    destroy: (asyncId: number): void => {
        if (store.has(asyncId)) {
            store.delete(asyncId);
        }
    }
});

asyncHook.enable();

const createRequestContext = (data: string, requestId = v4()): { requestId: string, data: string } => {
    const requestInfo = { requestId, data };
    store.set(asyncHooks.executionAsyncId(), requestInfo);
    return requestInfo;
};

const getRequestContext = (): { requestId: string, data: string }  => {
    const context = store.get(asyncHooks.executionAsyncId());
    return context || { requestId: null, data: null };
};

export { createRequestContext, getRequestContext };