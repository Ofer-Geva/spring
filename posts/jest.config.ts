import type {Config} from '@jest/types';
const config: Config.InitialOptions = {
    roots: ["<rootDir>/src/test"],
    testMatch: [
        "**/__tests__/**/*.+(ts|tsx|js)",
        "**/?(*.)+(spec|test).+(ts|tsx|js)",
    ],
    transform: {
        "^.+\\.(ts|tsx)$": "ts-jest",
    },
    preset: "ts-jest",
    testEnvironment: "node",
    coverageDirectory: "<rootDir>/src/test/coverage"
};

export default config;
