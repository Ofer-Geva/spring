export enum ReactionType {
    thumbsUp = "ThumbsUp",
    heart = "Heart",
    bravo = "Bravo",
    sad = "Sad",
    laugh = "Laugh",
    wow = "Wow"
}