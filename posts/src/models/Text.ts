import { ObjectId } from "mongodb";

export default class Text {
    private _id: string;
    private _user_id: string;
    private _text: string;
    private _reactions_by: Array<string>;
    private _created_date: Date;
    private _deleted: boolean;
    private _updated_date: Date;

    constructor(text: Text | object = {}) {
        this._id = text["id"] || text["_id"] || null;
        this._user_id = text["user_id"] || null;
        this._text = text["text"] || null;
        this._reactions_by = text["reactions_by"] || [];
        this._created_date = text["created_date"] || null;
        this._deleted = text["deleted"] || null;
        this._updated_date = text["updated_date"] || null;
    }

    get id(): string { return this._id }
    set id(v: string) { this._id = v }
    get user_id(): string { return this._user_id }
    set user_id(v: string) { this._user_id = v }
    get text(): string { return this._text }
    set text(v: string) { this._text = v }
    get reactions_by(): Array<string> { return this._reactions_by }
    set reactions_by(v: Array<string>) { this._reactions_by = v }
    get created_date(): Date { return this._created_date }
    set created_date(v: Date) { this._created_date = v }
    get deleted(): boolean { return this._deleted }
    set deleted(v: boolean) { this._deleted = v }
    get updated_date(): Date { return this._updated_date }
    set updated_date(v: Date) { this._updated_date = v }

    parseToDictionary = (): object => {
        return {
            id: this._id,
            user_id: this._user_id,
            text: this._text,
            reactions_by: this._reactions_by,
            created_date: this._created_date,
            deleted: this._deleted,
            updated_date: this._updated_date
        }
    }

    parseForDb = (): object => {
        return {
            user_id: new ObjectId(this._user_id),
            text: this._text,
            reactions_by: this._reactions_by,
            created_date: this._created_date,
            deleted: this._deleted,
            updated_date: this._updated_date
        }
    }

    parseToResponse = (): object => {
        return {
            id: this._id,
            user_id: this._user_id,
            text: this._text,
            created_date: this._created_date,
            deleted: this._deleted,
            updated_date: this._updated_date
        }
    }
}