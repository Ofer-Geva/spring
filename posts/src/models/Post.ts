import Text from './Text';

export default class Post extends Text {

    constructor(post: Post | object = {}) {
        super(post);
    }
}