import { ReactionType } from "./ReactionType";

export default class Reaction {
    type: ReactionType;
    user_id: string;
    created_date: Date;

    parseToDictionary = () => {
        return {
            type: this.type,
            user_id: this.user_id,
            created_date: this.created_date
        }
    }

    parseToResponse = () => {
        return {
            type: this.type,
            user_id: this.user_id,
            created_date: this.created_date
        }
    }

    parseForDb = () => {
        return {
            type: this.type,
            user_id: this.user_id,
            created_date: this.created_date
        }
    }
}