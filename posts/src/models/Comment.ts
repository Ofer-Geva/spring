import Text from './Text';

export default class Comment extends Text {
    private _post_id: string;
    private _replies: Array<Text>;

    constructor(comment: Comment | object = {}) {
        super(comment);
        this._replies = comment['replies'] || [];
        this._post_id = comment['post_id'] || null;
    }

    get post_id(): string { return this._post_id }
    set post_id(v: string) { this._post_id = v }
    get replies(): Array<Text> { return this._replies }
    set replies(v: Array<Text>) { this._replies = v }
}