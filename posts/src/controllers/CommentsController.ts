import { LambdaResponse, Reaction } from "../models";
import CommentsService from "../services/CommentsService";
import Logger from "../utilities/Logger";
import { Comment } from "../models";

export default class CommentsController {
	commentsService: CommentsService;

	constructor() {
		this.commentsService = new CommentsService();
	}

	getCommentById = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> getCommentById`);
			const comment_id:string = event.pathParameters.comment_id;
			const comment: Comment = await this.commentsService.getCommentById(comment_id);
			Logger.info(`CommentsController -> getCommentById, comments: ${JSON.stringify(comment)}`);
			const response: object = new LambdaResponse(200, { comment: comment.parseToResponse() }, {}).parseToDictionary();
			Logger.info(`CommentsController -> getCommentById, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> getCommentById - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	getAllCommentsForPost = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> getAllCommentsForPost`);
			const post_id:string = event.pathParameters.post_id;
			const comments: Array<Comment> = await this.commentsService.getAllCommentsForPost(post_id);
			Logger.info(`CommentsController -> getAllCommentsForPost, comments: ${JSON.stringify(comments)}`);
			const response: object = new LambdaResponse(200, { comments: comments.map(u => u.parseToResponse()) }, {}).parseToDictionary();
			Logger.info(`CommentsController -> getAllCommentsForPost, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> getAllCommentsForPost - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	reactToComment = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> reactToComment`);
			const comment_id:string = event.pathParameters.comment_id;
			const reaction: Reaction = await this.commentsService.reactToComment(event.body,comment_id);
			const response: object = new LambdaResponse(200, { comment: reaction.parseToResponse() }, {});
			Logger.info(`CommentsController -> reactToComment, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> reactToComment - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	createComment = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> createComment`);
			const comment: Comment = await this.commentsService.createComment(event.body);
			const response: object = new LambdaResponse(200, { comment: comment.parseToResponse() }, {});
			Logger.info(`CommentsController -> createComment, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> createComment - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	updateComment = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> updateComment`);
			const comment_id:string = event.pathParameters.comment_id;
			const comment: Comment = await this.commentsService.updateComment(comment_id,event.body);
			const response: object = new LambdaResponse(200, { comment: comment.parseToResponse() }, {});
			Logger.info(`CommentsController -> updateComment, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> updateComment - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	deleteComment = async (event): Promise<object> => {
		try {
			Logger.info(`CommentsController -> deleteComment`);
			const comment_id:string = event.pathParameters.comment_id;
			const comment: Comment = await this.commentsService.deleteComment(comment_id);
			const response: object = new LambdaResponse(200, { comment: comment.parseToResponse() }, {});
			Logger.info(`CommentsController -> deleteComment, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`CommentsController -> deleteComment - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}
}