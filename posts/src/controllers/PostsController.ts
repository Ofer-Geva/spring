import { LambdaResponse, Reaction } from "../models";
import PostsService from "../services/PostsService";
import Logger from "../utilities/Logger";
import { Post } from "../models";

export default class PostsController {
	postsService: PostsService;

	constructor() {
		this.postsService = new PostsService();
	}

	getPostById = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> getPostById`);
			const post_id:string = event.pathParameters.post_id;
			const post: Post = await this.postsService.getPostById(post_id);
			Logger.info(`PostsController -> getPostById, posts: ${JSON.stringify(post)}`);
			const response: object = new LambdaResponse(200, { post: post.parseToResponse() }, {}).parseToDictionary();
			Logger.info(`PostsController -> getPostById, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> getPostById - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	getAllPostsForUser = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> getAllPostsForUser`);
			const user_id:string = event.pathParameters.user_id;
			const posts: Array<Post> = await this.postsService.getAllPostsForUser(user_id);
			Logger.info(`PostsController -> getAllPostsForUser, posts: ${JSON.stringify(posts)}`);
			const response: object = new LambdaResponse(200, { posts: posts.map(u => u.parseToResponse()) }, {}).parseToDictionary();
			Logger.info(`PostsController -> getAllPostsForUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> getAllPostsForUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	reactToPost = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> reactToPost`);
			const post_id:string = event.pathParameters.post_id;
			const reaction: Reaction = await this.postsService.reactToPost(event.body, post_id);
			const response: object = new LambdaResponse(200, { reaction: reaction.parseToResponse() }, {});
			Logger.info(`PostsController -> reactToPost, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> reactToPost - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	createPost = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> createPost`);
			const post: Post = await this.postsService.createPost(event.body);
			const response: object = new LambdaResponse(200, { post: post.parseToResponse() }, {});
			Logger.info(`PostsController -> createPost, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> createPost - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	updatePost = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> updatePost`);
			const post_id:string = event.pathParameters.post_id;
			const post: Post = await this.postsService.updatePost(post_id,event.body);
			const response: object = new LambdaResponse(200, { post: post.parseToResponse() }, {});
			Logger.info(`PostsController -> updatePost, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> updatePost - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	deletePost = async (event): Promise<object> => {
		try {
			Logger.info(`PostsController -> deletePost`);
			const post_id:string = event.pathParameters.post_id;
			const post: Post = await this.postsService.deletePost(post_id);
			const response: object = new LambdaResponse(200, { post: post.parseToResponse() }, {});
			Logger.info(`PostsController -> deletePost, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`PostsController -> deletePost - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}
}