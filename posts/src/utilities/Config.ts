import MongoDbProvider from "../providers/MongoDbProvider"
import Logger from "./Logger";

export const Config = async ():Promise<object> => {
    Logger.info(`Config - initialize`);
    const mongoConnection = await MongoDbProvider.connect();
    const db = mongoConnection.db('spring');
    const collection = db.collection('config');
    const config = await collection.find().toArray();
    return config[0]
}