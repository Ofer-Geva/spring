import { MongoClient, ObjectId } from "mongodb"
import { Post, Reaction } from "../../models";
import MongoDbProvider from "../../providers/MongoDbProvider"
import Logger from "../../utilities/Logger";

export default class PostsDbService {
    private init = async () => {
        Logger.info(`PostsDbService -> init`);
        const client: MongoClient = await MongoDbProvider.connect();
        const db = client.db('spring');
        const collection = db.collection('posts');
        return collection;
    }

    getAllPostsForUser = async (user_id:string): Promise<Array<object>> => {
        Logger.info(`PostsDbService -> getAllPostsForUser`);
        try {
            const collection = await this.init();
            const ret = await collection.find({user_id:new ObjectId(user_id)}).toArray();
            Logger.info(`PostsDbService -> getAllPostsForUser - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`PostsDbService -> getAllPostsForUser - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    getPostById = async (post_id:string): Promise<object> => {
        Logger.info(`PostsDbService -> getPostById`);
        try {
            Logger.debug(`PostsDbService -> getPostById post_id: ${post_id}`);
            const collection = await this.init();
            const ret = await collection.findOne({_id:new ObjectId(post_id)});
            Logger.info(`PostsDbService -> getPostById - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`PostsDbService -> getPostById - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    createPost = async (post: Post): Promise<Post> => {
        Logger.info(`PostsDbService -> createPost`);
        try {
            const collection = await this.init();
            const ret = await collection.insertOne(post.parseForDb());
            Logger.info(`PostsDbService -> createPost - response: ${JSON.stringify(ret)}`);
            post.id = ret.acknowledged ? ret.insertedId.toHexString() : null;
            return post;
        } catch (err) {
            Logger.error(`PostsDbService -> createPost - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    updatePost = async (post: Post): Promise<Post> => {
        Logger.info(`PostsDbService -> updatePost`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(post.id)},{$set:post.parseForDb()});
            Logger.info(`PostsDbService -> updatePost - response: ${JSON.stringify(ret)}`);
            return post;
        } catch (err) {
            Logger.error(`PostsDbService -> updatePost - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    reactToPost = async (reaction:Reaction,post_id: string): Promise<boolean> => {
        Logger.info(`PostsDbService -> reactToPost`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(post_id)},{$push:{reactions_by:reaction.parseForDb()}});
            Logger.info(`PostsDbService -> reactToPost - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`PostsDbService -> reactToPost - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    deletePost = async (post_id: string): Promise<boolean> => {
        Logger.info(`PostsDbService -> deletePost`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(post_id)},{$set:{deleted:true}});
            Logger.info(`PostsDbService -> deletePost - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`PostsDbService -> deletePost - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }
}