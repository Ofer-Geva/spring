import { MongoClient, ObjectId } from "mongodb"
import { Comment, Reaction } from "../../models";
import MongoDbProvider from "../../providers/MongoDbProvider"
import Logger from "../../utilities/Logger";

export default class CommentsDbService {
    private init = async () => {
        Logger.info(`CommentsDbService -> init`);
        const client: MongoClient = await MongoDbProvider.connect();
        const db = client.db('spring');
        const collection = db.collection('comments');
        return collection;
    }

    getAllCommentsForPost = async (post_id:string): Promise<Array<object>> => {
        Logger.info(`CommentsDbService -> getAllCommentsForPost`);
        try {
            
            const collection = await this.init();
            const ret = await collection.find({_id: new ObjectId(post_id)}).toArray();
            Logger.info(`CommentsDbService -> getAllCommentsForPost - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`CommentsDbService -> getAllCommentsForPost - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    getCommentById = async (comment_id:string): Promise<object> => {
        Logger.info(`CommentsDbService -> getCommentById`);
        try {
            Logger.debug(`CommentsDbService -> getCommentById comment_id: ${comment_id}`);
            const collection = await this.init();
            const ret = await collection.findOne({_id:new ObjectId(comment_id)});
            Logger.info(`CommentsDbService -> getCommentById - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`CommentsDbService -> getCommentById - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    createComment = async (comment: Comment): Promise<Comment> => {
        Logger.info(`CommentsDbService -> createComment`);
        try {
            const collection = await this.init();
            const ret = await collection.insertOne(comment.parseForDb());
            Logger.info(`CommentsDbService -> createComment - response: ${JSON.stringify(ret)}`);
            comment.id = ret.acknowledged ? ret.insertedId.toHexString() : null;
            return comment;
        } catch (err) {
            Logger.error(`CommentsDbService -> createComment - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    updateComment = async (comment: Comment): Promise<Comment> => {
        Logger.info(`CommentsDbService -> updateComment`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(comment.id)},{$set:comment.parseForDb()});
            Logger.info(`CommentsDbService -> updateComment - response: ${JSON.stringify(ret)}`);
            return comment;
        } catch (err) {
            Logger.error(`CommentsDbService -> updateComment - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    reactToComment = async (reaction: Reaction, comment_id: string): Promise<boolean> => {
        Logger.info(`CommentsDbService -> reactToComment`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(comment_id)},{$push:{reactions_by:reaction.parseForDb()}});
            Logger.info(`CommentsDbService -> reactToComment - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`CommentsDbService -> reactToComment - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    deleteComment = async (comment_id: string): Promise<boolean> => {
        Logger.info(`CommentsDbService -> deleteComment`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(comment_id)},{$set:{deleted:true}});
            Logger.info(`CommentsDbService -> deleteComment - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`CommentsDbService -> deleteComment - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }
}