import Logger from "../utilities/Logger";
import PostsDbService from "./db/PostsDbService";
import { Post, Reaction } from "../models/";
import { ReactionType } from "../models/ReactionType";

export default class PostsService {
    postsDbService: PostsDbService;

    constructor() {
        this.postsDbService = new PostsDbService();
    }

    getPostById = async (post_id: string): Promise<Post> => {
        try {
            Logger.info(`PostsService -> getPostById`);
            Logger.debug(`PostsService -> getPostById post_id: ${post_id}`);
            const ret = await this.postsDbService.getPostById(post_id);
            const post: Post = new Post(ret);
            return post;
        }
        catch (err) {
            Logger.error(`PostsService -> getPostById - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    getAllPostsForUser = async (user_id: string): Promise<Array<Post>> => {
        try {
            Logger.info(`PostsService -> getAllPostsForUser`);
            Logger.debug(`PostsService -> getAllPostsForUser user_id: ${user_id}`);
            const ret = await this.postsDbService.getAllPostsForUser(user_id);
            Logger.verbose(`PostsService -> getAllPostsForUser - response: ${JSON.stringify(ret)}`);
            return ret.map(u => new Post(u));
        }
        catch (err) {
            Logger.error(`PostsService -> getAllPostsForUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    createPost = async (post_body: object): Promise<Post> => {
        try {
            Logger.info(`PostsService -> createPost - post_body: ${post_body}`);
            const post_body_parsed: object = typeof post_body === 'string' ? JSON.parse(post_body) : post_body;
            const post: Post = new Post(post_body_parsed);
            post.deleted = false;
            post.created_date = new Date();
            const ret = await this.postsDbService.createPost(post);
            Logger.debug(`PostsService -> createPost - post: ${JSON.stringify(post.parseForDb())}`);
            Logger.info(`PostsService -> createPost - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`PostsService -> createPost - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    reactToPost = async (reaction_body: object, post_id: string): Promise<Reaction> => {
        try {
            Logger.info(`PostsService -> reactToPost`);
            Logger.debug(`PostsService -> reactToPost reaction_body: ${JSON.stringify(reaction_body)}`);
            const reaction_body_parsed: object = typeof reaction_body === 'string' ? JSON.parse(reaction_body) : reaction_body;
            const reaction: Reaction = new Reaction();
            reaction.created_date = new Date();
            reaction.user_id = reaction_body_parsed['user_id'];
            reaction.type = ReactionType[reaction_body_parsed['type']];
            const ret = await this.postsDbService.reactToPost(reaction,post_id);
            Logger.debug(`PostsService -> reactToPost - reaction: ${JSON.stringify(reaction.parseForDb())}`);
            Logger.info(`PostsService -> reactToPost - response: ${JSON.stringify(ret)}`);
            return reaction;
        }
        catch (err) {
            Logger.error(`PostsService -> reactToPost - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    updatePost = async (post_id: string, post_body: object): Promise<Post> => {
        try {
            Logger.info(`PostsService -> updatePost - post_id: ${post_id}, post_body: ${post_body}`);
            const post_body_parsed: object = typeof post_body === 'string' ? JSON.parse(post_body) : post_body;
            const post_from_db = await this.postsDbService.getPostById(post_id);
            if(!post_from_db) throw new Error(`No post found for id ${post_id}`);
            const post:Post = new Post({...post_from_db, ...post_body_parsed});

            const ret = await this.postsDbService.updatePost(post);
            Logger.debug(`PostsService -> updatePost - post: ${JSON.stringify(post.parseToDictionary())}`);
            Logger.info(`PostsService -> updatePost - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`PostsService -> updatePost - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }
    
    deletePost = async (post_id: string): Promise<Post> => {
        try {
            Logger.info(`PostsService -> deletePost - post_id: ${post_id}`);
            const post_from_db = await this.postsDbService.getPostById(post_id);
            if(!post_from_db) throw new Error(`No post found for id ${post_id}`);
            const post:Post = new Post(post_from_db);
            if(post.deleted) throw new Error("Post already deleted");
            post.deleted = true;
            const ret = await this.postsDbService.deletePost(post_id);
            Logger.debug(`PostsService -> deletePost - post: ${JSON.stringify(post.parseToDictionary())}`);
            Logger.info(`PostsService -> deletePost - response: ${JSON.stringify(ret)}`);
            return post;
        }
        catch (err) {
            Logger.error(`PostsService -> deletePost - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }
}