import Logger from "../utilities/Logger";
import CommentsDbService from "./db/CommentsDbService";
import { Comment, Reaction } from "../models";
import { ReactionType } from "../models/ReactionType";

export default class CommentsService {
    commentsDbService: CommentsDbService;

    constructor() {
        this.commentsDbService = new CommentsDbService();
    }

    getCommentById = async (comment_id: string): Promise<Comment> => {
        try {
            Logger.info(`CommentsService -> getCommentById`);
            Logger.debug(`CommentsService -> getCommentById comment_id: ${comment_id}`);
            const ret = await this.commentsDbService.getCommentById(comment_id);
            const comment: Comment = new Comment(ret);
            return comment;
        }
        catch (err) {
            Logger.error(`CommentsService -> getCommentById - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    getAllCommentsForPost = async (post_id: string): Promise<Array<Comment>> => {
        try {
            Logger.info(`CommentsService -> getAllCommentsForPost`);
            Logger.debug(`PostsService -> getAllPostsForUser post_id: ${post_id}`);
            const ret = await this.commentsDbService.getAllCommentsForPost(post_id);
            Logger.verbose(`CommentsService -> getAllCommentsForPost - response: ${JSON.stringify(ret)}`);
            return ret.map(u => new Comment(u));
        }
        catch (err) {
            Logger.error(`CommentsService -> getAllCommentsForPost - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    createComment = async (comment_body: object): Promise<Comment> => {
        try {
            Logger.info(`CommentsService -> createComment - comment_body: ${comment_body}`);
            const comment_body_parsed: object = typeof comment_body === 'string' ? JSON.parse(comment_body) : comment_body;
            const comment: Comment = new Comment(comment_body_parsed);
            comment.deleted = false;
            comment.created_date = new Date();
            const ret = await this.commentsDbService.createComment(comment);
            Logger.debug(`CommentsService -> createComment - comment: ${JSON.stringify(comment.parseToDictionary())}`);
            Logger.info(`CommentsService -> createComment - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`CommentsService -> createComment - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    reactToComment = async (reaction_body: object, comment_id: string): Promise<Reaction> => {
        try {
            Logger.info(`CommentsService -> reactToComment`);
            Logger.debug(`CommentsService -> reactToComment reaction_body: ${JSON.stringify(reaction_body)}`);
            const reaction_body_parsed: object = typeof reaction_body === 'string' ? JSON.parse(reaction_body) : reaction_body;
            const reaction: Reaction = new Reaction();
            reaction.created_date = new Date();
            reaction.user_id = reaction_body_parsed['user_id'];
            reaction.type = ReactionType[reaction_body_parsed['type']];
            const ret = await this.commentsDbService.reactToComment(reaction,comment_id);
            Logger.debug(`CommentsService -> reactToComment - reaction: ${JSON.stringify(reaction.parseForDb())}`);
            Logger.info(`CommentsService -> reactToComment - response: ${JSON.stringify(ret)}`);
            return reaction;
        }
        catch (err) {
            Logger.error(`CommentsService -> reactToComment - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    updateComment = async (comment_id: string, comment_body: object): Promise<Comment> => {
        try {
            Logger.info(`CommentsService -> updateComment - comment_id: ${comment_id}, comment_body: ${comment_body}`);
            const comment_body_parsed: object = typeof comment_body === 'string' ? JSON.parse(comment_body) : comment_body;
            const comment_from_db = await this.commentsDbService.getCommentById(comment_id);
            if(!comment_from_db) throw new Error(`No comment found for id ${comment_id}`);
            const comment:Comment = new Comment(comment_from_db);
            const ret = await this.commentsDbService.updateComment(comment);
            Logger.debug(`CommentsService -> updateComment - comment: ${JSON.stringify(comment.parseToDictionary())}`);
            Logger.info(`CommentsService -> updateComment - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`CommentsService -> updateComment - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }
    
    deleteComment = async (comment_id: string): Promise<Comment> => {
        try {
            Logger.info(`CommentsService -> deleteComment - comment_id: ${comment_id}`);
            const comment_from_db = await this.commentsDbService.getCommentById(comment_id);
            if(!comment_from_db) throw new Error(`No comment found for id ${comment_id}`);
            const comment:Comment = new Comment(comment_from_db);
            if(comment.deleted) throw new Error("Comment already deleted");
            comment.deleted = true;
            const ret = await this.commentsDbService.deleteComment(comment_id);
            Logger.debug(`CommentsService -> deleteComment - comment: ${JSON.stringify(comment.parseToDictionary())}`);
            Logger.info(`CommentsService -> deleteComment - response: ${JSON.stringify(ret)}`);
            return comment;
        }
        catch (err) {
            Logger.error(`CommentsService -> deleteComment - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }
}