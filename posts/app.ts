require('dotenv').config();
import { Config } from "./src/utilities/Config";
import CommentsController from "./src/controllers/CommentsController";
import PostsController from "./src/controllers/PostsController";
import Logger from "./src/utilities/Logger";

export const posts = async (event) => {
    const config = await Config();
    global["config"] = config;
    const postsController: PostsController = new PostsController();
    Logger.info(`app -> posts`);
    switch (event.httpMethod) {
        case 'GET':
            if (event.pathParameters && event.pathParameters.post_id) {
                return await postsController.getPostById(event);
            } else {
                return await postsController.getAllPostsForUser(event);
            }
        case 'POST':
            if (event.path.includes('reactions')) {
                return await postsController.reactToPost(event);
            } else {
                return await postsController.createPost(event);
            }
        case 'PUT':
            return await postsController.updatePost(event);
        case 'DELETE':
            return await postsController.deletePost(event);
    }
};

export const comments = async (event) => {
    const config = await Config();
    global["config"] = config;
    const commentsController: CommentsController = new CommentsController();
    switch (event.httpMethod) {
        case 'GET':
            if (event.pathParameters && event.pathParameters.comment_id) {
                return await commentsController.getCommentById(event);
            } else {
                Logger.info(`app -> comments`);
                const ret = await commentsController.getAllCommentsForPost(event);
                Logger.info(`app -> comments - response: ${JSON.stringify(ret)}`);
                return ret;
            }
        case 'POST':
            if (event.path.includes('reactions')) {
                return await commentsController.reactToComment(event);
            } else {
                return await commentsController.createComment(event);
            }
        case 'PUT':
            return await commentsController.updateComment(event);
        case 'DELETE':
            return await commentsController.deleteComment(event);
    }
};
