import UsersController from "./src/controllers/UsersController";
import { Config } from "./src/utilities/Config";
import Logger from "./src/utilities/Logger";
require('dotenv').config();

export const users = async (event) => {
    const config = await Config();
    global["config"] = config;
    const usersController: UsersController = new UsersController();
    Logger.info(`app -> users`);
    switch(event.httpMethod) {
        case 'GET':
            if(event.pathParameters && event.pathParameters.user_id) {
                return await usersController.getUserById(event);
            } else {
                return await usersController.getAllUsers(event);
            }
        case 'POST':
            if(event.path.includes('login')) {
                return await usersController.loginUser(event);
            } else {
                return await usersController.createUser(event);
            }
        case 'PUT':
            if(event.path.includes('activate')) {
                return await usersController.activateUser(event);
            } else {
                return await usersController.updateUser(event);
            }
        case 'DELETE':
            return await usersController.deleteUser(event);
    }
};