import { LambdaResponse } from "../models";
import UsersService from "../services/UsersService";
import Logger from "../utilities/Logger";
import { User } from "../models";

export default class UsersController {
	usersService: UsersService;

	constructor() {
		this.usersService = new UsersService();
	}

	getUserById = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> getUserById`);
			const user_id:string = event.pathParameters.user_id;
			const user: User = await this.usersService.getUserById(user_id);
			Logger.info(`UsersController -> getUserById, users: ${JSON.stringify(user)}`);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {}).parseToDictionary();
			Logger.info(`UsersController -> getUserById, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> getUserById - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	getAllUsers = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> getAllUsers`);
			const users: Array<User> = await this.usersService.getAllUsers();
			Logger.info(`UsersController -> getAllUsers, users: ${JSON.stringify(users)}`);
			const response: object = new LambdaResponse(200, { users: users.map(u => u.parseToResponse()) }, {}).parseToDictionary();
			Logger.info(`UsersController -> getAllUsers, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> getAllUsers - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: typeof err === 'string' ? err : err.message }, {});
		}
	}

	loginUser = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> loginUser`);
			const user: User = await this.usersService.loginUser(event.body);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {});
			Logger.info(`UsersController -> loginUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> loginUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	createUser = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> createUser`);
			const user: User = await this.usersService.createUser(event.body);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {});
			Logger.info(`UsersController -> createUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> createUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	updateUser = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> updateUser`);
			const user_id:string = event.pathParameters.user_id;
			const user: User = await this.usersService.updateUser(user_id,event.body);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {});
			Logger.info(`UsersController -> updateUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> updateUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	activateUser = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> activateUser`);
			const user_id:string = event.pathParameters.user_id;
			const user: User = await this.usersService.activateUser(user_id);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {});
			Logger.info(`UsersController -> activateUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> activateUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}

	deleteUser = async (event): Promise<object> => {
		try {
			Logger.info(`UsersController -> deleteUser`);
			const user_id:string = event.pathParameters.user_id;
			const user: User = await this.usersService.deleteUser(user_id);
			const response: object = new LambdaResponse(200, { user: user.parseToResponse() }, {});
			Logger.info(`UsersController -> deleteUser, response: ${JSON.stringify(response)}`);
			return response;
		} catch (err) {
			Logger.error(`UsersController -> deleteUser - error: ${err.message ? err.message : err}`);
			return new LambdaResponse(500, { error: err.message ? err.message : err }, {});
		}
	}
}