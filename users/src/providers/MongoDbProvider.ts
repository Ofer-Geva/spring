import { MongoClient } from "mongodb";
require('dotenv').config();

export default class MongoDbProvider {
    static connection_string:string = process.env.MONGODB_CONNECTION_STRING;
    static db:string = process.env.MONGODB_DB;

    static connect = async ():Promise<MongoClient> => {
        const client = new MongoClient(MongoDbProvider.connection_string);
        await client.connect();
        return client;
    }
}
