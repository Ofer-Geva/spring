const winston = require('winston');
require('dotenv').config();

const myformat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

const Logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            level: "silly",
            format: myformat
        }),
        new winston.transports.File({
            level: 'warn',
            filename: `logs/${process.env.SERVER_NAME}.log`,
            format: winston.format.json()
        }),
    ]
});

export default Logger;