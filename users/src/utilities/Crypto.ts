import crypto = require('crypto');
import Logger from './Logger';

export class Crypto {
    static encryptIv = (text:string,key:string,iv_given:string = null):object => {
        Logger.info(`Crypto -> encryptIv`);
        const iv = iv_given ? Buffer.from(iv_given,'hex') : crypto.randomBytes(16);
        Logger.verbose(`Crypto -> encryptIv: iv ${iv.toString('hex')}, key ${key}, text ${text}`);
        const cipher = crypto.createCipheriv('aes-256-ctr',Buffer.from(key,'hex'),iv);
        const encrypted:string = Buffer.concat([cipher.update(text), cipher.final(), iv]).toString('hex');
        Logger.debug(`${JSON.stringify({iv: iv.toString('hex'), encrypted})}`);
        Logger.info(`Crypto -> encryptIv: return value: ${JSON.stringify({iv: iv.toString('hex'), encrypted})}`);
        return {iv: iv.toString('hex'), encrypted};
    }
}