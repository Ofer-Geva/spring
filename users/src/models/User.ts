export default class User {
    private _id: string;
    private _email: string;
    private _password: string;
    private _password_iv: string;
    private _handle: string;
    private _created_date: Date;
    private _active: boolean;
    private _deleted: boolean;
    private _last_login: Date;

    constructor(user: User | object = {}) {
        this._id = user["id"] || user["_id"] || null;
        this._email = user["email"] || null;
        this._password = user["password"] || null;
        this._password_iv = user["password_iv"] || null;
        this._handle = user["handle"] || null;
        this._created_date = user["created_date"] || null;
        this._active = user["active"] || null;
        this._deleted = user["deleted"] || null;
        this._last_login = user["last_login"] || null;
    }

    get id(): string { return this._id }
    set id(v: string) { this._id = v }
    get email(): string { return this._email }
    set email(v: string) { this._email = v }
    get password(): string { return this._password }
    set password(v: string) { this._password = v }
    get password_iv(): string { return this._password_iv }
    set password_iv(v: string) { this._password_iv = v }
    get handle(): string { return this._handle }
    set handle(v: string) { this._handle = v }
    get created_date(): Date { return this._created_date }
    set created_date(v: Date) { this._created_date = v }
    get active(): boolean { return this._active }
    set active(v: boolean) { this._active = v }
    get deleted(): boolean { return this._deleted }
    set deleted(v: boolean) { this._deleted = v }
    get last_login(): Date { return this._last_login }
    set last_login(v: Date) { this._last_login = v }

    parseToDictionary = (): object => {
        return {
            id: this._id,
            email: this._email,
            password: this._password,
            password_iv: this._password_iv,
            handle: this._handle,
            created_date: this._created_date,
            active: this._active,
            deleted: this._deleted,
            last_login: this._last_login
        }
    }

    parseForDb = (): object => {
        return {
            email: this._email,
            password: this._password,
            password_iv: this._password_iv,
            handle: this._handle,
            created_date: this._created_date,
            active: this._active,
            deleted: this._deleted,
            last_login: this._last_login
        }
    }

    parseToResponse = (): object => {
        return {
            id: this._id,
            email: this._email,
            handle: this._handle,
            created_date: this._created_date,
            active: this._active,
            deleted: this._deleted,
            last_login: this._last_login
        }
    }
}