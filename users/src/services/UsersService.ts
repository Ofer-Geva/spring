import Logger from "../utilities/Logger";
import UsersDbService from "./db/UsersDbService";
import { User } from "../models/";
import { Crypto } from "../utilities/Crypto";

export default class UsersService {
    usersDbService: UsersDbService;

    constructor() {
        this.usersDbService = new UsersDbService();
    }

    getUserById = async (user_id: string): Promise<User> => {
        try {
            Logger.info(`UsersService -> getUserById`);
            Logger.debug(`UsersService -> getUserById user_id: ${user_id}`);
            const ret = await this.usersDbService.getUserById(user_id);
            const user: User = new User(ret);
            return user;
        }
        catch (err) {
            Logger.error(`UsersService -> getUserById - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    getUserByEmail = async (user: User): Promise<boolean> => {
        try {
            Logger.info(`UsersService -> getUserByEmail`);
            Logger.debug(`UsersService -> getUserByEmail user: ${JSON.stringify(user.parseToDictionary())}`);
            const ret = await this.usersDbService.getUserByEmail(user);
            if (ret && ret["_id"]) return true;
            return false;
        }
        catch (err) {
            Logger.error(`UsersService -> getUserByEmail - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    getAllUsers = async (): Promise<Array<User>> => {
        try {
            Logger.info(`UsersService -> getAllUsers`);
            const ret = await this.usersDbService.getAllUsers();
            Logger.verbose(`UsersService -> getAllUsers - response: ${JSON.stringify(ret)}`);
            return ret.map(u => new User(u));
        }
        catch (err) {
            Logger.error(`UsersService -> getAllUsers - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    createUser = async (user_body: object): Promise<User> => {
        try {
            Logger.info(`UsersService -> createUser - user_body: ${user_body}`);
            const user_body_parsed: object = typeof user_body === 'string' ? JSON.parse(user_body) : user_body;
            const user: User = new User();
            const encrypted_password = Crypto.encryptIv(user_body_parsed['password'], global['config']['password_encryption_key']);
            Logger.debug(`UsersService -> createUser - encryopted: ${JSON.stringify({ encrypted_password })}`);
            user.email = user_body_parsed["email"];
            user.password_iv = encrypted_password["iv"];
            user.password = encrypted_password["encrypted"];
            user.active = false;
            user.deleted = false;
            user.created_date = new Date();
            user.handle = user_body_parsed["handle"];
            const user_exists = await this.getUserByEmail(user);
            if (user_exists) {
                throw new Error(`User ${user_body_parsed["email"]} already registered`);
            }
            const ret = await this.usersDbService.createUser(user);
            Logger.debug(`UsersService -> createUser - user: ${JSON.stringify(user.parseToDictionary())}`);
            Logger.info(`UsersService -> createUser - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`UsersService -> createUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    loginUser = async (user_body: object): Promise<User> => {
        try {
            Logger.info(`UsersService -> loginUser`);
            Logger.debug(`UsersService -> loginUser user_body: ${JSON.stringify(user_body)}`);
            const user_body_parsed: object = typeof user_body === 'string' ? JSON.parse(user_body) : user_body;
            const user: User = new User();
            user.email = user_body_parsed["email"];
            const user_exists = await this.usersDbService.getUserByEmail(user);
            if (!(user_exists && user_exists["_id"])) {
                throw new Error(`Incorrect email or password`);
            }
            Logger.info(`UsersService -> loginUser - user_exists: ${JSON.stringify(user_exists)}`);
            const existing_user: User = new User(user_exists);
            if(existing_user.deleted) throw new Error(`User ${existing_user.id} is deleted`);
            const encrypted_password = Crypto.encryptIv(user_body_parsed['password'], global['config']['password_encryption_key'], existing_user.password_iv);
            Logger.debug(`UsersService -> loginUser - encryopted: ${JSON.stringify({ encrypted_password })}`);
            if (encrypted_password['encrypted'] !== existing_user.password) {
                Logger.debug(`UsersService -> loginUser - encrypted_password !== existing_user.password: ${JSON.stringify({ encrypted_password, existing_user: existing_user.password })}`);
                throw new Error(`Incorrect email or password`);
            }
            return existing_user;
        }
        catch (err) {
            Logger.error(`UsersService -> loginUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    updateUser = async (user_id: string, user_body: object): Promise<User> => {
        try {
            Logger.info(`UsersService -> updateUser - user_id: ${user_id}, user_body: ${user_body}`);
            const user_body_parsed: object = typeof user_body === 'string' ? JSON.parse(user_body) : user_body;
            const user_from_db = await this.usersDbService.getUserById(user_id);
            if(!user_from_db) throw new Error(`No user found for id ${user_id}`);
            const user:User = new User(user_from_db);
            if (user_body_parsed['password']) {
                const encrypted_password = Crypto.encryptIv(user_body_parsed['password'], global['config']['password_encryption_key']);
                Logger.debug(`UsersService -> updateUser - encryopted: ${JSON.stringify({ encrypted_password })}`);
                if (user_body_parsed['email']) throw new Error("User email cannot be updated");
                user.password_iv = encrypted_password["iv"];
                user.password = encrypted_password["encrypted"];
            }
            if(user_body_parsed['handle']) user.handle = user_body_parsed["handle"];
            const ret = await this.usersDbService.updateUser(user);
            Logger.debug(`UsersService -> updateUser - user: ${JSON.stringify(user.parseToDictionary())}`);
            Logger.info(`UsersService -> updateUser - response: ${JSON.stringify(ret)}`);
            return ret;
        }
        catch (err) {
            Logger.error(`UsersService -> updateUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    activateUser = async (user_id: string): Promise<User> => {
        try {
            Logger.info(`UsersService -> activateUser - user_id: ${user_id}`);
            const user_from_db = await this.usersDbService.getUserById(user_id);
            if(!user_from_db) throw new Error(`No user found for id ${user_id}`);
            const user:User = new User(user_from_db);
            if(user.active) throw new Error("User already active");
            user.active = true;
            const ret = await this.usersDbService.activateUser(user_id);
            Logger.debug(`UsersService -> activateUser - user: ${JSON.stringify(user.parseToDictionary())}`);
            Logger.info(`UsersService -> activateUser - response: ${JSON.stringify(ret)}`);
            return user;
        }
        catch (err) {
            Logger.error(`UsersService -> activateUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }

    deleteUser = async (user_id: string): Promise<User> => {
        try {
            Logger.info(`UsersService -> deleteUser - user_id: ${user_id}`);
            const user_from_db = await this.usersDbService.getUserById(user_id);
            if(!user_from_db) throw new Error(`No user found for id ${user_id}`);
            const user:User = new User(user_from_db);
            if(user.deleted) throw new Error("User already deleted");
            user.deleted = true;
            const ret = await this.usersDbService.deleteUser(user_id);
            Logger.debug(`UsersService -> deleteUser - user: ${JSON.stringify(user.parseToDictionary())}`);
            Logger.info(`UsersService -> deleteUser - response: ${JSON.stringify(ret)}`);
            return user;
        }
        catch (err) {
            Logger.error(`UsersService -> deleteUser - error: ${err.message ? err.message : err}`);
            throw err;
        }
    }
}