import { MongoClient, ObjectId } from "mongodb"
import { User } from "../../models";
import MongoDbProvider from "../../providers/MongoDbProvider"
import Logger from "../../utilities/Logger";

export default class UsersDbService {
    private init = async () => {
        Logger.info(`UsersDbService -> init`);
        const client: MongoClient = await MongoDbProvider.connect();
        const db = client.db('spring');
        const collection = db.collection('users');
        return collection;
    }

    getAllUsers = async (): Promise<Array<object>> => {
        Logger.info(`UsersDbService -> getAllUsers`);
        try {
            const collection = await this.init();
            const ret = await collection.find().toArray();
            Logger.info(`UsersDbService -> getAllUsers - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`UsersDbService -> getAllUsers - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    getUserByEmail = async (user:User): Promise<object> => {
        Logger.info(`UsersDbService -> getUserByEmail`);
        try {
            Logger.debug(`UsersDbService -> getUserByEmail user: ${JSON.stringify(user.parseToDictionary())}`);
            const collection = await this.init();
            const ret = await collection.findOne({email:user.email});
            Logger.info(`UsersDbService -> getUserByEmail - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`UsersDbService -> getUserByEmail - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    getUserById = async (user_id:string): Promise<object> => {
        Logger.info(`UsersDbService -> getUserById`);
        try {
            Logger.debug(`UsersDbService -> getUserById user_id: ${user_id}`);
            const collection = await this.init();
            const ret = await collection.findOne({_id:new ObjectId(user_id)});
            Logger.info(`UsersDbService -> getUserById - response: ${JSON.stringify(ret)}`);
            return ret;
        } catch (err) {
            Logger.error(`UsersDbService -> getUserById - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    createUser = async (user: User): Promise<User> => {
        Logger.info(`UsersDbService -> createUser`);
        try {
            const collection = await this.init();
            const ret = await collection.insertOne(user.parseToDictionary());
            Logger.info(`UsersDbService -> createUser - response: ${JSON.stringify(ret)}`);
            user.id = ret.acknowledged ? ret.insertedId.toHexString() : null;
            return user;
        } catch (err) {
            Logger.error(`UsersDbService -> createUser - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    updateUser = async (user: User): Promise<User> => {
        Logger.info(`UsersDbService -> updateUser`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(user.id)},{$set:user.parseForUpdate()});
            Logger.info(`UsersDbService -> updateUser - response: ${JSON.stringify(ret)}`);
            return user;
        } catch (err) {
            Logger.error(`UsersDbService -> updateUser - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    activateUser = async (user_id: string): Promise<boolean> => {
        Logger.info(`UsersDbService -> activateUser`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(user_id)},{$set:{active:true}});
            Logger.info(`UsersDbService -> activateUser - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`UsersDbService -> activateUser - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }

    deleteUser = async (user_id: string): Promise<boolean> => {
        Logger.info(`UsersDbService -> deleteUser`);
        try {
            const collection = await this.init();
            const ret = await collection.updateOne({_id:new ObjectId(user_id)},{$set:{deleted:true}});
            Logger.info(`UsersDbService -> deleteUser - response: ${JSON.stringify(ret)}`);
            return true;
        } catch (err) {
            Logger.error(`UsersDbService -> deleteUser - error: ${JSON.stringify(err)}`);
            throw err;
        }
    }
}