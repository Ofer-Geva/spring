const express = require('express');
const users = require('./users/dist/app');
const posts = require('./posts/dist/app');
const PORT = 3000;

const event = {
    body: null,
    headers: {
        Accept: "*/*",
        Host: "localhost: 3000",
        "User-Agent": "insomnia/2021.4.1",
        "X-Forwarded-Port": "3000",
        "X-Forwarded-Proto": "http",
    },
    httpMethod: "GET",
    isBase64Encoded: false,
    multiValueHeaders: {
        Accept: ["X*/*"],
        Host: ["localhost: 3000"],
        "User-Agent": ["insomnia/2021.4.1"],
        "X-Forwarded-Port": ["3000"],
        "X-Forwarded-Proto": ["http"],
    },
    multiValueQueryStringParameters: null,
    path: null, 
    pathParameters: {},
    queryStringParameters: null,
    requestContext: {
        accountId: "123456789012",
        apiId: "1234567890",
        domainName: "localhost: 3000",
        extendedRequestId: null,
        httpMethod: "GET",
        identity: {
            accountId: null,
            apiKey: null,
            caller: null,
            cognitoAuthenticationProvider: null,
            cognitoAuthenticationType: null,
            cognitoIdentityPoolId: null,
            sourceIp: "127.0.0.1",
            user: null,
            userAgent: "CustomUserAgentString",
            userArn: null,
        },
        path: null,
        protocol: "HTTP/1.1",
        requestId: "bbe819ca-9da1-4ca5-836d-1f927cb160a5",
        requestTime: "05/Aug/2021: 19: 54: 57+0000",
        requestTimeEpoch: 1628193297,
        resourceId: "123456",
        resourcePath: null,
        stage: "Prod",
    },
    resource: null,
    stageVariables: null,
    version: "1.0",
}

const app = express();
app.use(express.json());
app.listen(PORT,() => {
    console.log("listening on port 3000");
});

const fillEvent = (req) => {
    event.path = req.path;
    event.resource = req.path;
    event.body = JSON.stringify(req.body);
    event.httpMethod = req.method;
    event.pathParameters = req.params;
    event.queryStringParameters = req.query;
    event.requestContext.httpMethod = req.method;
    event.requestContext.path = req.path;
    event.requestContext.resourcePath = req.path;
}

const usersRequestHandler = async (req,res) => {
    fillEvent(req);
    const response = await users.users(event);

    res.send(response);
}


const postsRequestHandler = async (req,res) => {
    fillEvent(req);
    const response = await posts.posts(event);

    res.send(response);
}

const commentsRequestHandler = async (req,res) => {
    fillEvent(req);
    const response = await posts.comments(event);

    res.send(response);
}

//Users
app.get('/users/',async (req,res) => usersRequestHandler(req,res));
app.get('/users/:user_id',async (req,res) => usersRequestHandler(req,res));
app.post('/users/',async (req,res) => usersRequestHandler(req,res));
app.post('/users/login',async (req,res) => usersRequestHandler(req,res));
app.put('/users/:user_id',async (req,res) => usersRequestHandler(req,res));
app.put('/users/activate/:user_id',async (req,res) => usersRequestHandler(req,res));
app.delete('/users/:user_id',async (req,res) => usersRequestHandler(req,res));

//Posts
app.get('/posts/users/:user_id',async (req,res) => postsRequestHandler(req,res));
app.get('/posts/:post_id',async (req,res) => postsRequestHandler(req,res));
app.post('/posts/',async (req,res) => postsRequestHandler(req,res));
app.post('/posts/:post_id/reactions',async (req,res) => postsRequestHandler(req,res));
app.put('/posts/:post_id',async (req,res) => postsRequestHandler(req,res));
app.delete('/posts/:post_id',async (req,res) => postsRequestHandler(req,res));
app.get('/posts/:post_id/comments',async (req,res) => commentsRequestHandler(req,res));
app.get('/posts/:post_id/comments/:comment_id',async (req,res) => commentsRequestHandler(req,res));
app.post('/posts/:post_id/comments',async (req,res) => commentsRequestHandler(req,res));
app.post('/posts/:post_id/comments/:comment_id/reactions',async (req,res) => commentsRequestHandler(req,res));
app.put('/posts/:post_id/comments/:comment_id',async (req,res) => commentsRequestHandler(req,res));
app.delete('/posts/:post_id/comments/:comment_id',async (req,res) => commentsRequestHandler(req,res));