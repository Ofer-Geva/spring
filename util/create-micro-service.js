const fs = require("fs");
const service_name = process.argv[2];

const createFiles = (dir, destination) => {
    for (let file of fs.readdirSync(dir)) {
        if(file.substr(-3) === ".js" || file.substr(-7) === ".js.map" || ['logs', 'dist', 'node_modules', 'null'].includes(file)) continue;
        if (fs.statSync(`${dir}/${file}`).isDirectory()) {
            if (!fs.existsSync(`${destination}/${file}`)) {
                fs.mkdirSync(`${destination}/${file}`);
            }
            createFiles(`${dir}/${file}`,`${destination}/${file}`);
        } else {
            console.log('created',file)
            let data = fs.readFileSync(`${dir}/${file}`,'utf-8');
            if (!fs.existsSync(`${destination}/${file}`))
                fs.writeFileSync(`${destination}/${file}`,data.replace(/(empty)/g,service_name));
        }
    }
};

if (fs.lstatSync(`../empty`).isDirectory()) {
    if (!fs.existsSync(`../${service_name}`)) {
        fs.mkdirSync(`../${service_name}`);
    }
    createFiles(`../empty`,`../${service_name}`);
} else {
    throw "No empty service";
}